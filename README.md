# JAWABAN PBO
### NO 1 

**ALGORITMA DESKRIPTIF**
1. Menampilkan menu selamat datang di Mamikos.
2. Menampilkan opsi menu: Login, Register, atau Keluar.
3. Meminta pengguna untuk memilih opsi menu.
4. Jika pengguna memilih Login atau Register:
   - Menampilkan opsi untuk memilih peran: Pemilik Kos atau Pencari Kos.
   - Meminta pengguna untuk memilih peran.
5. Jika pengguna memilih Login:
   - Meminta pengguna untuk memasukkan email dan password.
   - Jika peran yang dipilih adalah Pemilik Kos:
     - Memvalidasi masuknya pemilik kos dengan memanggil metode `signin()` pada objek `pemilik`.
     - Jika login berhasil, menampilkan pesan sukses dan menjalankan fungsi `run()` pada objek `pemilik`.
     - Jika login gagal, menampilkan pesan kesalahan.
   - Jika peran yang dipilih adalah Pencari Kos:
     - Memvalidasi masuknya pencari kos dengan memanggil metode `signin()` pada objek `user`.
     - Jika login berhasil, menampilkan pesan sukses dan menjalankan fungsi `run()` pada objek `user`.
     - Jika login gagal, menampilkan pesan kesalahan.
6. Jika pengguna memilih Register:
   - Meminta pengguna untuk memasukkan nama, email, dan password.
   - Jika peran yang dipilih adalah Pemilik Kos:
     - Memvalidasi pendaftaran pemilik kos dengan memanggil metode `signup()` pada objek `pemilik`.
     - Jika pendaftaran berhasil, menampilkan pesan sukses.
     - Jika pendaftaran gagal, menampilkan pesan kesalahan.
   - Jika peran yang dipilih adalah Pencari Kos:
     - Memvalidasi pendaftaran pencari kos dengan memanggil metode `signup()` pada objek `user`.
     - Jika pendaftaran berhasil, menampilkan pesan sukses.
     - Jika pendaftaran gagal, menampilkan pesan kesalahan.
7. Jika pengguna memilih Keluar, menampilkan pesan keluar dari Mamikos.
8. Mengulangi langkah-langkah di atas selama pengguna tidak memilih Keluar.
9. Menutup input.


**ALGORITMA JAVA**

![ALGORITMA](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/code_algoritma_1.png)

**ALGORITMA PSEUDOCODE**
![ALGORITMA](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/algoritma_pseudocode.png)

### NO 2
Berikut source code yang digunakan dalam program mamikos:

[Kos.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Kos/Kos.java)

[Main.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Main/Main.java)

[MamiKos.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Main/MamiKos.java)

[Antrean.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Sistem/Antrean.java)

[Sistem.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Sistem/Sistem.java)

[SistemPemilik.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Sistem/SistemPemilik.java)

[SistemPemilik.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Sistem/SistemPemilik.java)

[SistemUser.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/Sistem/SistemUser.java)

[PemilikKos.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/User/PemilikKos.java)

[User.java](https://github.com/dvndnazhrrr20/TUBES-PBO/blob/master/User/User.java)

### NO 3

KONSEP DASAR OOP ADA 4 PILAR YAITU : 
1. ABSRACT
2. INHERITANCE
3. ENCAPSULATION
4. POLYMORPHISM

KONSEP DASAR OOP ADA 4 YAITU :

OBJECT,CLASS,ATTRIBUTE,METHOD

1. OBJECT : Objek merupakan segala sesuatu yang ada di dunia nyata. Setiap objek memiliki atribut dan bisa melakukan tindakan (method). Objek adalah bentuk atau contoh nyata dari class. 
2. CLASS : Class merupakan template/kerangka untuk membuat objek. Class menggambarkan apa yang objek dapat lakukan dan bagaimana objek tersebut berinteraksi dengan objek lain. Class berupa kumpulan atas definisi data dan fungsi dalam suatu unit untuk suatu tujuan tertentu. 
3. ATTRIBUTE :  Attribute atau property adalah data yang membedakan antara objek satu dengan objek yang lainnya.
4. METHOD : Method atau disebut juga tingkah laku adalah hal-hal yang bisa dilakukan objek dari suatu class.
```
CONTOH 

Class: Motor.

Objek: Motor Beat, Motor Vario.

Atribut: Merk, warna, plat nomor.

Method: Maju, mundur, belok.
```


### NO 4

**ENCAPSULATION** 

Encapsulation adalah konsep dalam pemrograman berorientasi objek yang memungkinkan kita menggabungkan data dan perilaku terkait ke dalam sebuah unit tunggal yang disebut class. Ini membantu menjaga kerahasiaan data dan menyediakan antarmuka yang terdefinisi dengan jelas untuk berinteraksi dengan class tersebut. Dengan encapsulation, kita dapat menyembunyikan detail implementasi dari luar class, sehingga pengguna class hanya perlu tahu cara menggunakan antarmuka yang disediakan. Hal ini membantu mengurangi kompleksitas dan memudahkan pemeliharaan kode.
Selain itu, encapsulation juga memungkinkan kita menerapkan validasi dan logika tambahan saat mengakses atau memodifikasi data. 

- Atribut Private: Atribut-Atribut `emailUser`, `namaKos`, `lokasiKos`, `emailPemilikKos`, dan `status` dideklarasikan sebagai private. Hal ini berarti atribut-atribut tersebut tidak dapat diakses secara langsung dari luar class Antrean. Encapsulation menjaga kerahasiaan data dengan membatasi akses langsung ke atribut-atribut ini.
- Konstruktor: Terdapat konstruktor Antrean yang digunakan untuk menginisialisasi nilai atribut-atribut pada saat objek Antrean dibuat. Konstruktor ini menerima argumen `emailUser`, `namaKos`, `lokasiKos`, dan `emailPemilikKos`, yang digunakan untuk menginisialisasi nilai dari atribut-atribut tersebut.
- Metode Getters dan Setters: Terdapat metode-metode getters dan setters untuk setiap atribut, seperti `getEmailUser()`, `setEmailUser()`, `getNamaKos()`, dan `setNamaKos()`. Metode getters digunakan untuk mengakses nilai atribut, sedangkan metode setters digunakan untuk mengubah nilai atribut. Dalam hal ini, encapsulation memungkinkan kita untuk mengontrol bagaimana atribut-atribut tersebut diakses dan dimodifikasi, sehingga kita dapat menetapkan logika tambahan saat mengubah atau mengakses atribut.

Dengan menggunakan encapsulation, kita dapat menjaga privasi data dengan membatasi akses langsung ke atribut-atribut private. Kita juga dapat mengatur validasi atau logika tambahan saat mengubah atau mengakses atribut melalui metode-metode getters dan setters yang disediakan. Ini membantu menjaga integritas data dan memberikan kontrol yang lebih baik dalam penggunaan class Antrean

**SOURCE CODE**

![ENCAPSULATION](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/ENCAPSULATION.png)

Dalam Sourcecode diatas Penggunaan access modifiers private pada setiap atribut (emailUser, namaKos, lokasiKos, emailPemilikKos, dan status) mengenkapsulasi atribut-atribut tersebut, sehingga hanya dapat diakses dan diubah melalui metode yang disediakan dalam kelas Antrean.
Metode getter dan setter (getEmailUser, setEmailUser, getNamaKos, setNamaKos, getLokasiKos, setLokasiKos, getEmailPemilikKos, setEmailPemilikKos, getStatus, setStatus) digunakan untuk mendapatkan nilai dan mengubah nilai atribut dengan kontrol yang tepat. Dalam hal ini, hanya metode-metode tersebut yang dapat mengakses dan mengubah nilai atribut, sehingga menjaga keamanan data dan mencegah akses yang tidak sah.

### NO 5
**ABSRACTION**

Adalah cara untuk menyederhanakan informasi yang penting dari suatu objek atau sistem, dengan sementara menyembunyikan detail yang tidak relevan atau kompleks. Dalam OOP, Abstraction memungkinkan kita untuk fokus pada informasi penting seperti metode dan atribut yang relevan pada suatu objek, tanpa harus khawatir tentang bagaimana implementasinya di dalam kelas tersebut. Dengan demikian, Abstraction membantu kita untuk membagi kompleksitas menjadi bagian-bagian yang lebih kecil dan memahami objek atau sistem hanya dengan informasi yang diperlukan. Dalam praktiknya, Abstraction diterapkan dengan membuat kelas abstrak yang memiliki metode abstrak (yang tidak memiliki implementasi) untuk mewakili konsep umum, dan kemudian kelas turunan mewarisi kelas abstrak tersebut dan memberikan implementasi sesuai dengan detail khusus yang diperlukan.
Dengan menggunakan Abstraction, pemrograman dapat fokus pada pemecahan masalah dan desain yang lebih tinggi, sambil menyembunyikan detail yang kompleks atau tidak relevan, sehingga membuat pemrograman menjadi lebih terstruktur, modular, dan mudah dipahami.

**SOURCE CODE**

![ABSRACTION](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/SUPER_CLASS.png)

Dengan menggunakan kelas Sistem sebagai kerangka kerja, kita dapat memisahkan logika umum dari implementasi rinci yang spesifik untuk pemilik kos (SistemPemilik) atau (Sistem User) pencari kos. kelas Sistem memiliki beberapa metode abstrak yang harus diimplementasikan oleh kelas turunan. Metode-metode ini mencakup logika umum yang terkait dengan sistem, seperti proses login, signup, pengecekan keberadaan data, dan menjalankan sistem.



### NO 6

**INHERITANCE**

Inheritance dalam pemrograman berorientasi objek (OOP) adalah konsep yang memungkinkan kelas baru (kelas turunan) untuk mewarisi properti dan metode dari kelas yang sudah ada (kelas induk atau kelas super). Dalam inheritance, kelas turunan dapat memperluas atau mengkhususkan fungsionalitas yang sudah ada dalam kelas induknya.
Kelas turunan akan mewarisi semua properti dan metode yang tidak bersifat pribadi (private) dari kelas induknya. Properti dan metode yang diwarisi dapat digunakan kembali dalam kelas turunan tanpa perlu menulis ulang kode yang sama. Selain itu, kelas turunan juga dapat menambahkan properti dan metode baru atau mengubah perilaku metode yang diwarisi.

**SOURCECODE**


- Kelas abstrak menjadi superclass

![SUPERCLASS](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/SUPER_CLASS.png)

- Kelas SistemPemilik, dan SistemUser menjadi subclass nya
```
public class SistemPemilik extends Sistem {
    public static ArrayList<PemilikKos> pemilik;
    private PemilikKos logged;

    public SistemPemilik(){
        super();
        pemilik = new ArrayList<>();
        logged = null;
    }

    protected void showAntreanPemilik(String emailPemilik, boolean all){
        int count = 0;
        for(Antrean antrean : waitingList){
            if(emailPemilik.equals(antrean.getEmailPemilikKos())){
                if(all){
                    count += 1;
                    System.out.println(count + ". " + antrean.getNamaKos() + " - " + antrean.getLokasiKos() + " oleh " + SistemUser.getNameUser(antrean.getEmailUser()) + " (" + antrean.getEmailUser() + ")");
                }else{
                    if(antrean.getStatus() == 0){
                        count += 1;
                        System.out.println(count + ". " + antrean.getNamaKos() + " - " + antrean.getLokasiKos() + " oleh " + SistemUser.getNameUser(antrean.getEmailUser()) + " (" + antrean.getEmailUser() + ")");
                    }
                }
            }
        }

        if(count == 0){
            System.out.println("Tidak ada waiting list pemesanan kos");
        }
    }

    protected int getCountWaitingStatusUser(String emailPemilik){
        int count = 0;
        for(Antrean antrean : waitingList){
            if(emailPemilik.equals(antrean.getEmailPemilikKos()) && antrean.getStatus() == 0){
                count += 1;
            }
        }
        return count;
    }

    protected void updateStatusWaiting(String emailPemilik, int index, int status){
        int count = 1;
        for(int i=0; i<waitingList.size(); i++){
            if(emailPemilik.equals(waitingList.get(i).getEmailPemilikKos()) && waitingList.get(i).getStatus() == 0){
                if(index == count){
                    waitingList.get(i).setStatus(status);
                    break;
                }
                count += 1;
            }
        }
    }

    private Antrean getAntrean(String email, int index, int status){
        int count = 1;
        for(int i=0; i<waitingList.size(); i++){
            if(email.equals(waitingList.get(i).getEmailPemilikKos()) && waitingList.get(i).getStatus() == status){
                if(index == count){
                    return waitingList.get(i);
                }
                count += 1;
            }
        }
        return null;
    }

    public static void showKosByLocation(String lokasi){
        int count = 0;
        for(PemilikKos pemilikKos : pemilik){
            ArrayList<Kos> kosList = pemilikKos.getKos();

            for(Kos kos : kosList){
                if(kos.getLokasi().equals(lokasi)){
                    count += 1;

                    if(count == 1){
                        System.out.println("List Kos Sekitar " + lokasi + " :");
                    }else{
                        System.out.println();
                    }

                    System.out.println(count + ". " + kos.getNamaKosan());
                    System.out.println("Fasilitas :");
                    for(String fasilitas : kos.getFasilitas()){
                        System.out.println("- " + fasilitas);
                    }
                    System.out.println("Tersedia : " + kos.getTersedia());
                    System.out.println("Harga : " + kos.getHarga());
                }
            }
        }

        if(count == 0){
            System.out.println("Tidak Ada Kos di Sekitar " + lokasi);
        }
    }

    public static void showKosByLocationTersedia(String lokasi){
        int count = 0;
        for(PemilikKos pemilikKos : pemilik){
            ArrayList<Kos> kosList = pemilikKos.getKos();

            for(Kos kos : kosList){
                if(kos.getLokasi().equals(lokasi) && kos.getTersedia() > 0){
                    count += 1;

                    if(count == 1){
                        System.out.println("List Kos Sekitar " + lokasi + " :");
                    }

                    System.out.println(count + ". " + kos.getNamaKosan() + " (" + kos.getTersedia() + " kamar tersedia)");
                }
            }
        }

        if(count == 0){
            System.out.println("Tidak Ada Kos di Sekitar " + lokasi);
        }
    }

    public static int getCountKosByLocationTersedia(String lokasi){
        int count = 0;
        for(PemilikKos pemilikKos : pemilik){
            ArrayList<Kos> kosList = pemilikKos.getKos();

            for(Kos kos : kosList){
                if(kos.getLokasi().equals(lokasi) && kos.getTersedia() > 0){
                    count += 1;
                }
            }
        }

        return count;
    }

    public static Antrean getKosByLocationIndexKos(String lokasi, int index){
        int count = 0;
        for(PemilikKos pemilikKos : pemilik){
            ArrayList<Kos> kosList = pemilikKos.getKos();

            for(Kos kos : kosList){
                if(kos.getLokasi().equals(lokasi) && kos.getTersedia() > 0){
                    count += 1;

                    if(index == count){
                        return new Antrean(null, kos.getNamaKosan(), kos.getLokasi(), pemilikKos.getEmail());
                    }
                }
            }
        }

        return null;
    }

    @Override
    public void run(){
        int menu, kosIndex, status, confirmFasilitas, indexLokasi;
        Kos kos;

        do{
            System.out.println("=== MENU PEMILIK MAMIKOS ===");
            System.out.println("1. Tambah Kos");
            System.out.println("2. Update Status Pesan Kos");
            System.out.println("3. Status Semua Pesanan Kos");
            System.out.println("4. Logout");
            System.out.print("Pilih >> ");
            menu = Integer.parseInt(input.nextLine());

            switch(menu){
                case 1:{
                    kos = new Kos();

                    do{
                        System.out.print("Masukkan Nama Kos : ");
                        kos.setNamaKosan(input.nextLine());
                    }while(kos.getNamaKosan().equals(""));

                    System.out.println("Lokasi Kos :");
                    for(int i=0; i<LOKASI.length; i++){
                        System.out.println((i+1) + ". " + LOKASI[i]);
                    }
                    do{
                        System.out.print("Pilih >> ");
                        indexLokasi = Integer.parseInt(input.nextLine());
                    }while(indexLokasi < 1 || indexLokasi > LOKASI.length);

                    kos.setLokasi(LOKASI[indexLokasi-1]);

                    if(!logged.isKosExist(kos.getNamaKosan(), kos.getLokasi())){
                        do{
                            System.out.print("Masukkan Total Kamar Kos : ");
                            kos.setKamar(Integer.parseInt(input.nextLine()));
                        }while(kos.getKamar() < 1);

                        for(String f : FASILITAS){
                            do{
                                System.out.print("Apakah ada fasilitas " + f + " [1 : Iya | 2 : Tidak] :");
                                confirmFasilitas = Integer.parseInt(input.nextLine());
                            }while(confirmFasilitas != 1 && confirmFasilitas != 2);

                            if(confirmFasilitas == 1){
                                kos.addFasilitas(f);
                            }
                        }

                        do{
                            System.out.print("Masukkan Harga Kos : ");
                            kos.setHarga(Long.parseLong(input.nextLine()));
                        }while(kos.getHarga() < 1);

                        logged.addKos(kos);
                        System.out.println("\nBerhasil menambah kos baru");
                    }else{
                        System.out.println("\nGagal menambah kos. Kos sudah terdaftar");
                    }

                    break;
                }

                case 2:{
                    if(getCountWaitingStatusUser(logged.getEmail()) > 0){
                        showAntreanPemilik(logged.getEmail(), false);
                        System.out.print("Pilih >> ");
                        kosIndex = Integer.parseInt(input.nextLine());

                        if(kosIndex >= 1 && kosIndex <= getCountWaitingStatusUser(logged.getEmail())){
                            do{
                                System.out.print("Ubah Status [1 : Terima | 2 : Tolak] : ");
                                status = Integer.parseInt(input.nextLine());
                            }while(status < 1 || status > 2);

                            updateStatusWaiting(logged.getEmail(), kosIndex, status);
                            if(status == 1){
                                Antrean tempAntrean = getAntrean(logged.getEmail(), kosIndex, status);

                                kos = logged.getKos(tempAntrean.getNamaKos(), tempAntrean.getLokasiKos());
                                kos.setTerisi(kos.getTerisi() + 1);

                                logged.updateKos(kos);
                            }
                            System.out.println("Berhasil mengubah status pemesanan kos");
                        }else{
                            System.out.println("Pilihan User Pemesan Tidak Valid!");
                        }
                    }else{
                        System.out.println("Tidak ada pencari kos yang sedang melakukan pemesanan");
                    }

                    break;
                }

                case 3:{
                    showAntreanPemilik(logged.getEmail(), true);

                    break;
                }

                case 4:{
                    System.out.println("Keluar dari pemilik mamikos...");
                    logged = null;

                    break;
                }

                default:{
                    System.out.println("Menu tidak valid");

                    break;
                }
            }

            if(menu != 4){
                System.out.println();
            }
        }while(menu != 4);
    }

    @Override
    public boolean isExist(String email) {
        for(PemilikKos p : pemilik){
            if(p.getEmail().equals(email)){
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean signup(String nama, String email, String password) {
        if(!isExist(email)){
            pemilik.add(new PemilikKos(nama, email, password));
            return true;
        }
        
        return false;
    }

    @Override
    public boolean signin(String email, String password) {
        for(PemilikKos p : pemilik){
            if(p.getEmail().equals(email) && p.getPassword().equals(password)){
                logged = p;
                return true;
            }
        }

        return false;
    }
}
```
![SISTEM USER](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/sistem_user.png)



**POLYMORPHISM**

Polymorphism dalam OOP adalah konsep di mana objek dapat memiliki banyak bentuk atau tipe yang berbeda. Dengan menggunakan polimorfisme, objek dapat dianggap sebagai objek dari kelas yang lebih umum atau sebagai instansi dari kelas turunan yang lebih spesifik. Hal ini memungkinkan pemanggilan metode yang sama dengan perilaku yang berbeda tergantung pada objek yang sebenarnya. Polymorphism meningkatkan fleksibilitas, modularitas, dan keterbacaan kode dalam pemrograman berorientasi objek.

**SOURCECODE**

![POLYMORPHISM](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/polyyy.png)

Di dalam contoh diatas ada sebuah superclass Sistem yang memiliki empat metode abstrak: run(), isExist(), signup(), dan signin(). Metode-metode ini tidak memiliki implementasi di dalam superclass dan harus diimplementasikan di kelas-kelas anak yang mewarisinya.
Kemudian, terdapat dua kelas anak, yaitu SistemUser dan SistemPemilikKos, yang merupakan subclass dari Sistem. Kedua kelas ini meng-extend superclass Sistem dan mengimplementasikan metode-metode abstrak yang didefinisikan di dalamnya.

### No 7

Mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP

- Usecase User: Mewakili pengguna aplikasi Mamikos. Memiliki atribut email dan password untuk autentikasi login.

- Usecase PemilikKos: Turunan dari kelas User. Mewakili pemilik kosan yang dapat mendaftarkan kosannya. Memiliki atribut seperti nama kosan, lokasi, fasilitas-fasilitas kosan, dan harga.

- Usecase PenggunaKos: Turunan dari kelas User. Mewakili pengguna yang dapat mencari dan memesan kosan. Memiliki atribut untuk menyimpan data pemesanan.

- Usecase Kosan: Mewakili kosan yang terdaftar dalam aplikasi. Memiliki atribut seperti nama kosan, lokasi, fasilitas-fasilitas kosan, dan harga.

- Usecase Antrean: Mewakili antrian pemesanan kosan. Berfungsi untuk menghubungkan antara pengguna yang memesan dengan pemilik kosan. Memiliki atribut seperti email pengguna, nama kosan, lokasi, email pemilik kosan, dan status pemesanan.

- Usecase Sistem: Berfungsi sebagai pengelola utama program. Menyediakan metode untuk sign up, login, pendaftaran kosan, pencarian kosan, pemesanan kosan, dan pembayaran. Menggunakan ArrayList untuk menyimpan data pengguna, pemilik kosan, kosan, dan antrean.

### NO 8
Menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table 

**USE CASE TABLE**

![USECASE](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/USE_CASE_PRIORITAS.png)


**CLASS DIAGRAM**


![CLASS DIAGRAM](https://gitlab.com/dvndnazhr20/pbo/-/raw/main/ss/MamikosClassDiagram.png)

### NO 9

[IMPLEMENTASI SEDERHANA OOP](https://youtu.be/yVtGZkvDGas)

### NO 10

use case yang dibuat dalam program :

1. Use Case: Log In
   - Aktor: Pengguna
   - Tujuan: Mengakses akun pengguna atau pemilik kosan
   - Langkah-langkah:
     1. Pengguna memilih opsi "Pengguna Kosan" atau "Pemilik Kosan" pada halaman awal.
     2. Pengguna memasukkan email dan password.
     3. Sistem memeriksa kecocokan email dan password dengan data yang tersimpan.
     4. Jika sesuai, pengguna berhasil masuk ke akun pengguna atau pemilik kosan.
     5. Jika tidak sesuai, pengguna diminta untuk mencoba lagi atau melakukan pendaftaran.

2. Use Case: Pendaftaran Kosan
   - Aktor: Pemilik Kosan
   - Tujuan: Mendaftarkan kosan ke dalam aplikasi
   - Langkah-langkah:
     1. Pemilik kosan yang telah masuk ke akun pemilik kosan memilih opsi "Pendaftaran Kosan" pada halaman utama.
     2. Pemilik kosan mengisi informasi tentang kosannya seperti nama kosan, lokasi kosan, fasilitas-fasilitas yang ada di dalam kosan, dan harga.
     3. Data kosan ditambahkan ke dalam daftar kosan yang tersedia di aplikasi.

3. Use Case: Pemesanan Kosan
   - Aktor: Pengguna
   - Tujuan: Memesan kosan yang tersedia
   - Langkah-langkah:
     1. Pengguna yang telah masuk ke akun pengguna memilih opsi "Pemesanan Kosan" pada halaman utama.
     2. Pengguna memilih lokasi kosan yang diinginkan dari pilihan yang tersedia.
     3. Sistem menampilkan daftar kosan yang tersedia di lokasi tersebut.
     4. Pengguna memilih kosan yang ingin dipesan.
     5. Pemilik kosan menerima notifikasi pemesanan.
     6. Pemilik kosan dapat menyetujui atau menolak pemesanan tersebut.

4. Use Case: Pembayaran
   - Aktor: Pengguna
   - Tujuan: Melakukan pembayaran sewa kosan
   - Langkah-langkah:
     1. Setelah pemesanan kosan disetujui oleh pemilik kosan, pengguna memilih opsi "Pembayaran" pada halaman utama.
     2. Pengguna melakukan pembayaran secara tunai kepada pemilik kosan.

Use case ini mencakup aktivitas log in, pendaftaran kosan, pemesanan kosan, dan pembayaran.

[SETELAH DI RUN](https://youtu.be/QiGmopWfOsI)
